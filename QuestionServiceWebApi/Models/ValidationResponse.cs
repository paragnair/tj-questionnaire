﻿namespace QuestionServiceWebApi.Models
{
    public class ValidationResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        internal static ValidationResponse CreateError(string message)
        {
            return new ValidationResponse() { Message = message };
        }

        internal static ValidationResponse CreateSuccess()
        {
            return new ValidationResponse() { Success = true };
        }
    }
}
﻿using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Interfaces;
using QuestionServiceWebApi.Models;

namespace QuestionServiceWebApi.Services
{
    public class ValidationService : IValidationService
    {
        public ValidationResponse ValidateAnswers(SubmissionRequest request)
        {   
            foreach (var question in request.Questions)
            {
                if (string.IsNullOrEmpty(question.Answer))
                {
                    return ValidationResponse.CreateError(string.Format("Question {0} is missing an answer", question.Id));
                }
            }

            return ValidationResponse.CreateSuccess();
        }
    }
}
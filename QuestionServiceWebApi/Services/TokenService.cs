﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuestionServiceWebApi.Interfaces;

namespace QuestionServiceWebApi.Services
{
    public class TokenService : ITokenService
    {
        public string CreateToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
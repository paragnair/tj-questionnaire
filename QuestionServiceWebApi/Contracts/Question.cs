using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace QuestionServiceWebApi.Contracts
{
    [DataContract]
    public class Question : Entity
    {

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public QuestionType Type { get; set; }

        [DataMember]
        public IList<Option> Options { get; set; }

        [DataMember]
        public string Answer { get; set; }

        internal static Question CreateQuestion(int id, string question)
        {
            // default to descriptive text
            return CreateQuestion(id, question, QuestionType.DescriptiveText);
        }

        internal static Question CreateQuestion(int id, string question, QuestionType questionType)
        {
            return new Question()
            {
                Id = id,
                QuestionText = question,
                Type = questionType
            };
        }

        internal static Question CreateQuestion(int id, string question, QuestionType questionsType, string[] options)
        {
            var questionInstance = CreateQuestion(id, question, questionsType);
            questionInstance.Options = CreateOptions(options.Select(x => x.Split('|')));
            return questionInstance;
        }

        private static IList<Option> CreateOptions(IEnumerable<string[]> options)
        {
            return options.Select(x => new Option() { Display = x[0], Value = x[1] }).ToList();
        }
    }

    public class Option
    {
        public string Display { get; set; }

        public string Value { get; set; }
    }
}
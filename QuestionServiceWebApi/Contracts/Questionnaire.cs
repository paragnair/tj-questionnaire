using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QuestionServiceWebApi.Contracts
{
    [DataContract]
    public class Questionnaire : Entity
    {
        [DataMember]
        public string QuestionnaireTitle { get; set; }

        //[DataMember]
        //public IList<string> QuestionsText { get; set; }

        [DataMember]
        public IList<Question> Questions { get; set; }
    }
}
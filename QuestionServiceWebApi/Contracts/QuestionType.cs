using System.Runtime.Serialization;

namespace QuestionServiceWebApi.Contracts
{
    [DataContract]
    public enum QuestionType
    {
        None = 0,
        [EnumMember]
        ShortText = 1,
        [EnumMember]
        DescriptiveText = 2,
        [EnumMember]
        MultipleChoice = 3,
        [EnumMember]
        SingleOptionChoice = 4
    }
}
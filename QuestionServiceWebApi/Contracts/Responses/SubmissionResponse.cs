﻿namespace QuestionServiceWebApi.Contracts.Responses
{
    public class SubmissionResponse : StatusResponse
    {
        public string Token { get; set; }

        public static SubmissionResponse CreateError(string message)
        {
            return new SubmissionResponse()
            {
                Success = false,
                Message = message
            };
        }

        public static SubmissionResponse CreateSuccess(string token)
        {
            return new SubmissionResponse()
            {
                Success = true,
                Token = token
            };
        }
    }
}
﻿namespace QuestionServiceWebApi.Contracts.Responses
{
    public abstract class StatusResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestionServiceWebApi.Contracts.Requests
{
    public class SubmissionRequest
    {
        public List<Question> Questions { get; set; }
    }
}
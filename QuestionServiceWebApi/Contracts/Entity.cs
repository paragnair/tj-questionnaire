using System.Runtime.Serialization;

namespace QuestionServiceWebApi.Contracts
{
    [DataContract]
    public abstract class Entity
    {
        [DataMember]
        public int Id { get; set; }
    }
}
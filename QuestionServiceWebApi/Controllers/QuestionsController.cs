﻿using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Contracts.Responses;
using QuestionServiceWebApi.Interfaces;
using QuestionServiceWebApi.Repositories;
using System.Web.Http;
using System.Web.Http.Cors;
using QuestionServiceWebApi.Services;

namespace QuestionServiceWebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class QuestionsController : ApiController
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IValidationService _validationService;
        private ITokenService _tokenService;

        public QuestionsController(IQuestionRepository questionRepository): this(questionRepository, new ValidationService(), new TokenService())
        {
            
        }

        public QuestionsController() : this(new QuestionRepository(), new ValidationService(), new TokenService())
        {
        }

        public QuestionsController(IQuestionRepository questionRepository, IValidationService validationService, ITokenService tokenService)
        {
            _questionRepository = questionRepository;
            _validationService = validationService;
            _tokenService = tokenService;
        }

        // GET api/questions
        public Questionnaire Get()
        {
            return _questionRepository.GetQuestionnaire();
        }

        // POST api/questions
        public SubmissionResponse Post([FromBody]SubmissionRequest request)
        {
            // validate the request
            var validationResponse = _validationService.ValidateAnswers(request);
            if (validationResponse.Success)
            {
                // generate a token
                var token = _tokenService.CreateToken();

                // pass the token while saving
                var response = _questionRepository.SaveAnswers(token, request);

                if (response)
                {
                    return SubmissionResponse.CreateSuccess(token);
                }
                else
                {
                    return SubmissionResponse.CreateError("Database Error");
                }
            }
            else
            {
                return SubmissionResponse.CreateError(validationResponse.Message);
            }
        }
    }
}

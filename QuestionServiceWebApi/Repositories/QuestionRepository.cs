using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace QuestionServiceWebApi.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private static Questionnaire _questionnaire = new Questionnaire
        {
            QuestionnaireTitle = "Geography Questions",
            Questions = new List<Question>
            {
                Question.CreateQuestion(1, "What is the capital of Cuba?", QuestionType.ShortText),
                Question.CreateQuestion(2, "What is the capital of France?", QuestionType.DescriptiveText),
                Question.CreateQuestion(3, "What is the capital of Poland?", QuestionType.MultipleChoice,
                    new string[] {"Wroclaw|Wroclaw", "WarSaw|WarSaw"}),
                Question.CreateQuestion(4, "What is the capital of Germany?", QuestionType.SingleOptionChoice,
                    new string[] {"Augsburg|AB", "Munich|MN", "Frankfurt|FRA"})
            }
        };

        private static Dictionary<string, Dictionary<int, string>> _answers;

        static QuestionRepository()
        {
            _answers = new Dictionary<string, Dictionary<int, string>>();
        }

        public Questionnaire GetQuestionnaire()
        {
            return _questionnaire;
        }

        public bool SaveAnswers(string token, SubmissionRequest request)
        {
            try
            {
                var answers = request.Questions.ToDictionary(k => k.Id, v => v.Answer);
                if (_answers.ContainsKey(token))
                {
                    // overwrite answers
                    _answers[token] = answers;
                }
                else
                {
                    // add new
                    _answers.Add(token, answers);
                }
                return true;
            }
            catch
            {
                //TODO: Log error
                return false;
            }
        }
    }
}
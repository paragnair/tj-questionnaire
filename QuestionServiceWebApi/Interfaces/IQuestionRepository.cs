using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;

namespace QuestionServiceWebApi.Interfaces
{
    public interface IQuestionRepository
    {
        Questionnaire GetQuestionnaire();
        
        bool SaveAnswers(string token, SubmissionRequest request);
    }
}
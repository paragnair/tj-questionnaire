﻿using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Models;

namespace QuestionServiceWebApi.Interfaces
{
    public interface IValidationService
    {
        ValidationResponse ValidateAnswers(SubmissionRequest request);
    }
}

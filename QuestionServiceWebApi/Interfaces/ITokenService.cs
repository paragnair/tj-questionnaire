﻿namespace QuestionServiceWebApi.Interfaces
{
    public interface ITokenService
    {
        string CreateToken();
    }
}

﻿using Moq;
using NUnit.Framework;
using PairingTest.Unit.Tests.QuestionServiceWebApi.Stubs;
using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Controllers;
using QuestionServiceWebApi.Interfaces;
using QuestionServiceWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    [TestFixture]
    [Category("Service Controller")]
    public class QuestionsControllerTests
    {
        [Test]
        public void ShouldGetQuestions()
        {
            //Arrange
            var expectedTitle = "My expected questions";
            var expectedQuestions = new Questionnaire() { QuestionnaireTitle = expectedTitle };
            var fakeQuestionRepository = new FakeQuestionRepository() { ExpectedQuestions = expectedQuestions };
            var questionsController = new QuestionsController(fakeQuestionRepository);

            //Act
            var questions = questionsController.Get();

            //Assert
            Assert.That(questions.QuestionnaireTitle, Is.EqualTo(expectedTitle));
        }

        [Test]
        public void QuestionsShouldHaveIdAndType()
        {
            // Arrange
            var expectedId = 1;
            var expectedType = QuestionType.ShortText;

            var expectedQuestions = new Questionnaire()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = expectedId, QuestionText = "Question 1", Type = expectedType }
                }

            };
            var fakeQuestionRepository = new FakeQuestionRepository() { ExpectedQuestions = expectedQuestions };
            var questionnaireController = new QuestionsController(fakeQuestionRepository);

            // Act
            var questions = questionnaireController.Get();

            // Assert
            var candidate = questions.Questions[0];
            Assert.That(candidate.Id, Is.Not.Null);
            Assert.That(candidate.Id, Is.EqualTo(expectedId));
            Assert.That(candidate.Type, Is.Not.Null);
            Assert.That(candidate.Type, Is.EqualTo(expectedType));
        }

        [Test]
        public void MultipleChoiceQuestions_ShouldHaveOptions()
        {
            // Arrange
            var expectedId = 1;
            var expectedType = QuestionType.MultipleChoice;
            var expectedOptionCount = 2;
            var expectedQuestions = new Questionnaire()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = expectedId, QuestionText = "Question 1", Type = expectedType, Options = new List<Option>()
                    {
                        new Option() { Display = "One", Value = "1" },
                        new Option() { Display = "Two", Value = "2" }
                    }}
                }
            };
            var fakeQuestionRepository = new FakeQuestionRepository() { ExpectedQuestions = expectedQuestions };
            var questionnaireController = new QuestionsController(fakeQuestionRepository);

            // Act
            var questions = questionnaireController.Get();

            // Assert
            var candidate = questions.Questions.FirstOrDefault();
            Assert.That(candidate.Type, Is.EqualTo(expectedType));
            Assert.That(candidate.Options, Is.Not.Null);
            Assert.That(candidate.Options.Count, Is.EqualTo(expectedOptionCount));
        }

        [Test]
        public void SingleOptionChoiceQuestions_ShouldHaveOptions()
        {
            // Arrange
            var expectedId = 1;
            var expectedType = QuestionType.SingleOptionChoice;
            var expectedOptionCount = 2;
            var expectedQuestions = new Questionnaire()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = expectedId, QuestionText = "Question 1", Type = expectedType, Options = new List<Option>()
                    {
                        new Option() { Display = "One", Value = "1" },
                        new Option() { Display = "Two", Value = "2" }
                    }}
                }
            };
            var fakeQuestionRepository = new FakeQuestionRepository() { ExpectedQuestions = expectedQuestions };
            var questionnaireController = new QuestionsController(fakeQuestionRepository);

            // Act
            var questions = questionnaireController.Get();

            // Assert
            var candidate = questions.Questions.FirstOrDefault();
            Assert.That(candidate.Type, Is.EqualTo(expectedType));
            Assert.That(candidate.Options, Is.Not.Null);
            Assert.That(candidate.Options.Count, Is.EqualTo(expectedOptionCount));
        }

        [Test]
        public void SuccessfulSubmission_ShouldReturnToken()
        {
            // Arrange
            var expectedToken = Guid.NewGuid().ToString();
            var expectedValidationResponse = new ValidationResponse() { Success = true };
            var expectedSaveAnswerResponse = true;

            var step = 0;
            Mock<IValidationService> validationServiceMock = new Mock<IValidationService>();
            validationServiceMock.Setup(x => x.ValidateAnswers(It.IsAny<SubmissionRequest>()))
                .Returns(expectedValidationResponse)
                .Callback(() => Assert.That(step++, Is.EqualTo(0)));


            Mock<ITokenService> tokenServiceMock = new Mock<ITokenService>();
            tokenServiceMock.Setup(x => x.CreateToken())
                .Returns(expectedToken)
                .Callback(() => Assert.That(step++, Is.EqualTo(1)));

            Mock<IQuestionRepository> questionRepositoryMock = new Mock<IQuestionRepository>();
            questionRepositoryMock.Setup(x => x.SaveAnswers(It.IsAny<string>(), It.IsAny<SubmissionRequest>()))
                .Returns(expectedSaveAnswerResponse)
                .Callback(() => Assert.That(step++, Is.EqualTo(2)));



            var questionnaireController = new QuestionsController(questionRepositoryMock.Object, validationServiceMock.Object, tokenServiceMock.Object);
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Answer = "First" },
                    new Question() { Id = 2, Answer = "A,B" }
                }
            };

            // Act
            var response = questionnaireController.Post(request);

            // Assert
            validationServiceMock.VerifyAll();
            questionRepositoryMock.VerifyAll();
            tokenServiceMock.VerifyAll();

            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.True);
            Assert.That(response.Token, Is.EqualTo(expectedToken));
        }

        [Test]
        public void UnSuccessfulSubmission_Validation_ShouldReturnErrorMessage()
        {
            // Arrange
            var expectedToken = Guid.NewGuid().ToString();
            var expectedValidationResponse = new ValidationResponse() { Success = false, Message = "Some error" };
            var expectedSaveAnswerResponse = true;

            Mock<ITokenService> tokenServiceMock = new Mock<ITokenService>();
            tokenServiceMock.Setup(x => x.CreateToken()).Returns(expectedToken);

            Mock<IQuestionRepository> questionRepositoryMock = new Mock<IQuestionRepository>();
            Mock<IValidationService> validationServiceMock = new Mock<IValidationService>();
            validationServiceMock.Setup(x => x.ValidateAnswers(It.IsAny<SubmissionRequest>())).Returns(expectedValidationResponse);

            questionRepositoryMock.Setup(x => x.SaveAnswers(It.IsAny<string>(), It.IsAny<SubmissionRequest>()))
                .Returns(expectedSaveAnswerResponse);
            var questionnaireController = new QuestionsController(questionRepositoryMock.Object, validationServiceMock.Object, tokenServiceMock.Object);
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Answer = "First" },
                    new Question() { Id = 2, Answer = "A,B" }
                }
            };

            // Act
            var response = questionnaireController.Post(request);

            // Assert
            validationServiceMock.VerifyAll();
            questionRepositoryMock.Verify(x => x.SaveAnswers(It.IsAny<string>(), It.IsAny<SubmissionRequest>()), Times.Never);
            tokenServiceMock.Verify(x => x.CreateToken(), Times.Never);

            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.False);
            Assert.That(response.Message, Is.Not.Null);
            Assert.That(response.Message, Is.EqualTo(expectedValidationResponse.Message));
        }
    }
}
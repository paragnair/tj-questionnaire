﻿using NUnit.Framework;
using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Interfaces;
using QuestionServiceWebApi.Services;
using System.Collections.Generic;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    [TestFixture]
    [Category("Service")]
    public class ValidationServiceTests
    {
        [Test]
        public void ValidateAnswers_ShortText_EmptyAnswer_ShouldReturnFalse()
        {
            // Arrange
            var expectedSuccess = false;
            var expectedMessage = "Question 1 is missing an answer";
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Type = QuestionType.ShortText }
                }
            };

            // Act
            IValidationService validationService = new ValidationService();
            var response = validationService.ValidateAnswers(request);

            // Assert
            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.EqualTo(expectedSuccess));
            Assert.That(response.Message, Is.Not.Null);
            Assert.That(response.Message, Is.EqualTo(expectedMessage));
        }

        [Test]
        public void ValidateAnswers_DescriptiveText_EmptyAnswer_ShouldReturnFalse()
        {
            // Arrange
            var expectedSuccess = false;
            var expectedMessage = "Question 1 is missing an answer";
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Type = QuestionType.DescriptiveText }
                }
            };

            // Act
            IValidationService validationService = new ValidationService();
            var response = validationService.ValidateAnswers(request);

            // Assert
            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.EqualTo(expectedSuccess));
            Assert.That(response.Message, Is.Not.Null);
            Assert.That(response.Message, Is.EqualTo(expectedMessage));
        }

        [Test]
        public void ValidateAnswers_MultipleChoice_EmptyAnswer_ShouldReturnFalse()
        {
            // Arrange
            var expectedSuccess = false;
            var expectedMessage = "Question 1 is missing an answer";
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Type = QuestionType.MultipleChoice }
                }
            };

            // Act
            IValidationService validationService = new ValidationService();
            var response = validationService.ValidateAnswers(request);

            // Assert
            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.EqualTo(expectedSuccess));
            Assert.That(response.Message, Is.Not.Null);
            Assert.That(response.Message, Is.EqualTo(expectedMessage));
        }

        [Test]
        public void ValidateAnswers_SingleOptionChoice_EmptyAnswer_ShouldReturnFalse()
        {
            // Arrange
            var expectedSuccess = false;
            var expectedMessage = "Question 1 is missing an answer";
            var request = new SubmissionRequest()
            {
                Questions = new List<Question>()
                {
                    new Question() { Id = 1, Type = QuestionType.SingleOptionChoice }
                }
            };

            // Act
            IValidationService validationService = new ValidationService();
            var response = validationService.ValidateAnswers(request);

            // Assert
            Assert.That(response, Is.Not.Null);
            Assert.That(response.Success, Is.EqualTo(expectedSuccess));
            Assert.That(response.Message, Is.Not.Null);
            Assert.That(response.Message, Is.EqualTo(expectedMessage));
        }
    }
}

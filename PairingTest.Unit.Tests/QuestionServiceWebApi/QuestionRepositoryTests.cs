﻿using NUnit.Framework;
using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Repositories;
using System.Linq;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi
{
    [TestFixture]
    [Category("Service Repository")]
    public class QuestionRepositoryTests
    {
        [Test]
        public void ShouldGetExpectedQuestionnaire()
        {
            // Arrange
            var questionRepository = new QuestionRepository();

            // Act
            var questionnaire = questionRepository.GetQuestionnaire();

            // Assert
            Assert.That(questionnaire.QuestionnaireTitle, Is.EqualTo("Geography Questions"));
            Assert.That(questionnaire.Questions[0].QuestionText, Is.EqualTo("What is the capital of Cuba?"));
            Assert.That(questionnaire.Questions[1].QuestionText, Is.EqualTo("What is the capital of France?"));
            Assert.That(questionnaire.Questions[2].QuestionText, Is.EqualTo("What is the capital of Poland?"));
            Assert.That(questionnaire.Questions[3].QuestionText, Is.EqualTo("What is the capital of Germany?"));
        }

        [Test]
        public void ShouldGetQuestionsWithIdAndType()
        {
            // Arrange
            var questionRepository = new QuestionRepository();

            // Act
            var questionnaire = questionRepository.GetQuestionnaire();

            // Assert
            foreach (var question in questionnaire.Questions)
            {
                Assert.That(question.Id, Is.Not.Null);
                Assert.That(question.Id, Is.GreaterThanOrEqualTo(1));
                Assert.That(question.Type, Is.Not.EqualTo(QuestionType.None));
            }
        }

        [Test]
        public void MultipleChoiceQuestions_ShouldHaveOptions()
        {
            // Arrange
            var questionRepository = new QuestionRepository();

            // Act
            var questionnaire = questionRepository.GetQuestionnaire();

            // Assert
            var questions = questionnaire.Questions.Where(x => x.Type == QuestionType.MultipleChoice);

            foreach (var question in questions)
            {
                Assert.That(question.Options, Is.Not.Null);
                // There should atleast be 2 options
                Assert.That(question.Options.Count, Is.GreaterThanOrEqualTo(2));
            }
        }

        [Test]
        public void SingleOptionChoiceQuestions_ShouldHaveOptions()
        {
            // Arrange
            var questionRepository = new QuestionRepository();

            // Act
            var questionnaire = questionRepository.GetQuestionnaire();

            // Assert
            var questions = questionnaire.Questions.Where(x => x.Type == QuestionType.SingleOptionChoice);

            foreach (var question in questions)
            {
                Assert.That(question.Options, Is.Not.Null);
                // There should atleast be 2 options
                Assert.That(question.Options.Count, Is.GreaterThanOrEqualTo(2));
            }
        }
    }
}
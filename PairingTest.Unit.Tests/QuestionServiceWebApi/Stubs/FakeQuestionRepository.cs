﻿using QuestionServiceWebApi.Contracts;
using QuestionServiceWebApi.Contracts.Requests;
using QuestionServiceWebApi.Interfaces;

namespace PairingTest.Unit.Tests.QuestionServiceWebApi.Stubs
{
    public class FakeQuestionRepository : IQuestionRepository
    {
        public Questionnaire ExpectedQuestions { get; set; }
        public Questionnaire GetQuestionnaire()
        {
            return ExpectedQuestions;
        }

        public bool ExpectedSaveAnswersResponse { get; set; }
        public bool SaveAnswers(string token, SubmissionRequest request)
        {
            return ExpectedSaveAnswersResponse;
        }
    }
}
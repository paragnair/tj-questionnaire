﻿using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using PairingTest.Web.ViewModelFactories.Interfaces;
using PairingTest.Web.ViewModelServices;
using PairingTest.Web.ViewModelServices.Interfaces;

namespace PairingTest.Unit.Tests.Web
{
    [TestFixture]
    [Category("Web ViewModelService")]
    public class QuestionnaireViewModelServiceTests
    {
        [Test]
        public void ShouldGetQuestionnairePageViewModel()
        {
            // Arrange
            var expectedTitle = "My expected quesitons";
            var expectedServiceUrl = "foo.com";
            Mock<IConfigFactory> configFactoryMock = new Mock<IConfigFactory>();
            configFactoryMock.Setup(x => x.ServiceURL).Returns(expectedServiceUrl);

            // Act
            IQuestionnaireViewModelService service = new QuestionnaireViewModelService(configFactoryMock.Object);
            var result = service.GetQuestionnairePageViewModel();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.QuestionnaireTitle, Is.EqualTo(expectedTitle));
            Assert.That(result.ServiceURL, Is.EqualTo(expectedServiceUrl));
        }
    }
}
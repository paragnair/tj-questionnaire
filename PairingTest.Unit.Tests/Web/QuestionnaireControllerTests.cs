﻿using Moq;
using NUnit.Framework;
using PairingTest.Web.Controllers;
using PairingTest.Web.Models;
using PairingTest.Web.ViewModelServices.Interfaces;

namespace PairingTest.Unit.Tests.Web
{
    [TestFixture]
    [Category("Web Controller")]
    public class QuestionnaireControllerTests
    {
        [Test]
        public void ShouldGetQuestions()
        {
            //Arrange
            var expectedTitle = "My expected quesitons";
            var expectedServiceUrl = "foo.com";
            var expectedQuestionnaireViewModel = new QuestionnaireViewModel()
            {
                QuestionnaireTitle = expectedTitle,
                ServiceURL = expectedServiceUrl
            };

            Mock<IQuestionnaireViewModelService> questionnaireViewModelServiceMock = new Mock<IQuestionnaireViewModelService>();
            questionnaireViewModelServiceMock.Setup(x => x.GetQuestionnairePageViewModel())
                .Returns(expectedQuestionnaireViewModel);

            var questionnaireController = new QuestionnaireController(questionnaireViewModelServiceMock.Object);

            //Act
            var result = (QuestionnaireViewModel)questionnaireController.Index().ViewData.Model;

            //Assert
            Assert.That(result.QuestionnaireTitle, Is.EqualTo(expectedTitle));
            Assert.That(result.ServiceURL, Is.EqualTo(expectedServiceUrl));
        }
    }
}
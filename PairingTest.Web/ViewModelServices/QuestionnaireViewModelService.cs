﻿using PairingTest.Web.Models;
using PairingTest.Web.ViewModelFactories;
using PairingTest.Web.ViewModelFactories.Interfaces;
using PairingTest.Web.ViewModelServices.Interfaces;

namespace PairingTest.Web.ViewModelServices
{
    public class QuestionnaireViewModelService : IQuestionnaireViewModelService
    {
        private readonly IConfigFactory _configFactory;

        public QuestionnaireViewModelService(IConfigFactory configFactory)
        {
            _configFactory = configFactory;
        }

        public QuestionnaireViewModelService() : this(new ConfigFactory())
        {

        }

        public QuestionnaireViewModel GetQuestionnairePageViewModel()
        {

            return new QuestionnaireViewModel()
            {
                QuestionnaireTitle = "My expected quesitons",
                ServiceURL = _configFactory.ServiceURL
            };
        }
    }
}
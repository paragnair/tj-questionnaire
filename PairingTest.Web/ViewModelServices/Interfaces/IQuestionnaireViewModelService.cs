﻿using PairingTest.Web.Models;

namespace PairingTest.Web.ViewModelServices.Interfaces
{
    public interface IQuestionnaireViewModelService
    {
        QuestionnaireViewModel GetQuestionnairePageViewModel();
    }
}

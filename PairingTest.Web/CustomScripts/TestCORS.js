﻿var li = $('<li>');

function success(testName) {
    var elem = li.clone().addClass('success').html(testName + ' succeeded');
    $('#test-results').append(elem);
}

function failure(testName, error) {
    var elem = li.clone().addClass('failure').html(testName + ' failed: ' + error);
    $('#test-results').append(elem);
}

// test CORS
function testCORS() {
    var url = "http://localhost:50014/api/Questions";

    $.ajax({
        type: 'get',
        url: url,
        success: function (data, text) {
            success('CORS');
        },
        error: function (request, status, error) {
            failure('CORS', status);
        }
    });
}

function runAllTests() {
    testCORS();
}

$(document).ready(function () {
    runAllTests();
    
});
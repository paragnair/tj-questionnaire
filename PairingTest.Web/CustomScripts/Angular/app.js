﻿var app = angular.module('questionnaire', ['ngRoute', 'checklist-model']);
var baseTempaltePath = '/angular/templates/';

// configure data services
app.factory('DataServices', ['$http', function ($http) {
    var baseUrl = $('[data-service-url]').attr('data-service-url');
    var getQuestions = function () {
        return $http.get(baseUrl);
    };
    var saveAnswers = function (answers) {
        var data = {
            questions: []
        };

        for (var i = answers.length - 1; i >= 0; i--) {
            data.questions.push({
                id: answers[i].id,
                answer: answers[i].answer
            });
        }

        return $http.post(baseUrl, data);
    };

    return {
        getQuestions: getQuestions,
        saveAnswers: saveAnswers
    };
}]);

app.factory('ValidationService', [function () {
    var validateAnswers = function (answers) {

        var errors = [];
        for (var i = answers.length - 1; i >= 0; i--) {
            if (answers[i].answer == null || answers[i].answer == '') {
                errors.push(answers[i].id);
            }
        }
        return {
            isValid: errors.length === 0,
            errors: errors
        };
    };

    return {
        validateAnswers: validateAnswers
    };
}]);

// configure paths
app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: baseTempaltePath + 'questionnaire.tpl.html',
        controller: 'QuestionnaireController'
    });
});

// directives
app.directive('control', function ($compile) {

    var getTemplate = function (controlType, id, options) {
        switch (controlType) {
            case 1:
                return '<input class="form-control col-md-12" type="text" name="answers[' + id + '].answer" ng-model="content.answer" required />';
            case 3:
                return '<checkbox-list content="content"></checkbox-list>';
            case 4:
                return '<radio-list content="content"></radio-list>';
            default:
                return '<textarea class="form-control" rows="3" name="answer_' + id + '" ng-model="content.answer"></textarea>';
        }
    };

    var linker = function (scope, element, attrs) {
        element.html(getTemplate(scope.content.type, scope.content.id, scope.content.options)).show();

        var e = $compile(element.contents())(scope);
        element.replaceWith(e);
    };

    return {
        restrict: 'E',
        link: linker,
        scope: {
            content: '='
        }
    };
});

app.directive('checkboxList', function () {
    return {
        restrict: 'E',
        templateUrl: baseTempaltePath + 'checkboxlist.tpl.html',
        replace: true,
        scope: {
            content: '='
        },
        controller: [
            '$scope', function ($scope) {
                $scope.answers = {
                    value: []
                };
            }
        ]
    };
});

app.directive('radioList', function () {
    return {
        restrict: 'E',
        templateUrl: baseTempaltePath + 'radiolist.tpl.html',
        replace: true,
        scope: {
            content: '='
        }
    };
});

// controllers
app.controller('QuestionnaireController', [
    '$scope', 'DataServices', 'ValidationService', function ($scope, DataServices, ValidationService) {
        $scope.title = '';
        $scope.questions = [];
        $scope.isLoading = true;
        $scope.debug = window.location.hash.indexOf('debug') >= 0;
        $scope.thankYou = false;

        $scope.fetchQuestions = function () {
            DataServices.getQuestions().then(function (response) {
                if (response.status === 200) {
                    var data = response.data;
                    $scope.title = data.questionnaireTitle;
                    $scope.questions = data.questions;
                }
                $scope.isLoading = false;
            });
        }

        $scope.submitAnswers = function () {
            // get answers
            var answers = fetchAnswers();

            // validate answers
            var validation = ValidationService.validateAnswers(answers);
            var handleSuccess = function (response) {
                var token = response.token;
                //TODO: Cookie the user to restrict submitting answers multiple times

                // close questionnaire
                $scope.thankYou = true;

                toastr["success"]("Yohoo! Your answers were submitted");
            };
            var handleFailure = function (response) {
                console.log(response);
                toastr["error"](response.message);
            };

            if (validation.isValid) {
                // submit answers
                DataServices.saveAnswers(answers)
                .then(function (response) {
                    if (response.status === 200 && response.data.success) {
                        handleSuccess(response.data);
                    } else if (response.status === 200 && !response.data.success) {
                        handleFailure(response.data);
                    } else {
                        handleFailure({ message: "Oops! We did a boo boo. The technical team has been reported this error and they are working on a resolution now." });
                    }
                });


            } else {
                // add errors to questions
                for (var i = $scope.questions.length - 1; i >= 0; i--) {
                    if ($.inArray($scope.questions[i].id, validation.errors) >= 0) {
                        $scope.questions[i].isValid = false;
                    } else {
                        $scope.questions[i].isValid = true;
                    }
                }
                toastr["error"]("All answers are required");
            }
        }

        var fetchAnswers = function () {
            var answers = [];
            for (var i = $scope.questions.length - 1; i >= 0; i--) {
                var question = $scope.questions[i];
                var answer = {
                    id: question.id,
                    answer: getAnswer(question)
                };
                answers.push(answer);
            }
            return answers;
        };

        var getAnswer = function (question) {
            switch (question.type) {
                case 3:
                    return question.answers != null ? question.answers.join() : '';
                default:
                    return question.answer;
            }
        };

        $scope.fetchQuestions();
    }
]);

app.controller('MultipleChoiceController', ['$scope', function ($scope) {
    $scope.answers = {
        value: []
    };
}]);
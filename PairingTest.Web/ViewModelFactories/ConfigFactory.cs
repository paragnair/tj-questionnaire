﻿using PairingTest.Web.ViewModelFactories.Interfaces;
using System.Configuration;

namespace PairingTest.Web.ViewModelFactories
{
    public class ConfigFactory : IConfigFactory
    {
        public string ServiceURL { get; private set; }

        private const string KEY_QuestionnaireServiceUri = "QuestionnaireServiceUri";

        public ConfigFactory()
        {
            if (ConfigurationManager.AppSettings[KEY_QuestionnaireServiceUri] != null)
            {
                ServiceURL = ConfigurationManager.AppSettings[KEY_QuestionnaireServiceUri];
            }
        }
    }
}
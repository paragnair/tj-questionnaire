﻿namespace PairingTest.Web.ViewModelFactories.Interfaces
{
    public interface IConfigFactory
    {
        string ServiceURL { get; }
    }
}

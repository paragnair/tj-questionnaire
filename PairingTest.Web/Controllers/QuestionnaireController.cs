﻿using PairingTest.Web.ViewModelServices;
using PairingTest.Web.ViewModelServices.Interfaces;
using System.Web.Mvc;

namespace PairingTest.Web.Controllers
{
    public partial class QuestionnaireController : Controller
    {
        private readonly IQuestionnaireViewModelService _questionnaireViewModelService;


        public QuestionnaireController() : this(new QuestionnaireViewModelService())
        {

        }
        public QuestionnaireController(IQuestionnaireViewModelService questionnaireViewModelService)
        {
            _questionnaireViewModelService = questionnaireViewModelService;
        }

        /* ASYNC ACTION METHOD... IF REQUIRED... */
        //        public async Task<ViewResult> Index()
        //        {
        //        }

        public virtual ViewResult Index()
        {
            var viewModel = _questionnaireViewModelService.GetQuestionnairePageViewModel();

            return View(viewModel);
        }
    }
}
